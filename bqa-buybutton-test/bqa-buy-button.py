# 1.Please use pycharm
# 2.please download below package(ex: pip install xxxx):
# selenium, contextmanager, BeautifulSoup, urlopen, json, pandas, datetime

from contextlib import contextmanager
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
from urllib.request import urlopen
import time
import json
import pandas as pd
from datetime import datetime

@contextmanager
def create_chrome_driver() -> Chrome:
    driver=Chrome()
    yield driver
    driver.quit()

#step1: 撈取所有URL
url_b2c_sitemap="https://www.benq.com/en-us/sitemap.xml"
url_b2c_all=[]

def get_all_b2c_url(driver: Chrome):
    html = urlopen(url_b2c_sitemap)
    bsObj = BeautifulSoup(html, features="xml")
    for url_b2c in bsObj.findAll("url"):
        cell = url_b2c.find("loc")
        url=cell.contents[0]
        url_b2c_all.append(url)
        # print(cell.contents)
    # print(url_b2c_all)


url_zowie_sitemap="https://zowie.benq.com/en-us/sitemap.xml"
url_zowie_all=[]
def get_all_zowie_url(driver: Chrome):
    html = urlopen(url_zowie_sitemap)
    bsObj = BeautifulSoup(html, features="xml")
    for url_zowie in bsObj.findAll("url"):
        cell = url_zowie.find("loc")
        url = cell.contents[0]
        url_zowie_all.append(url)
        # print(cell.contents)
    # print(url_zowie_all)

#step2: 過濾URL
all_b2c_g6_url=[]
def filter_all_b2c_g6_url(driver: Chrome):
    all_b2c_g6_url_JSON=[]
    for i in range(0,len(url_b2c_all)):
        url=url_b2c_all[i]
        publishUrl = "buy.html"
        if url.find("buy.html")!=-1 and url.find("campaign")<1 or url.find("/monitor/accessory/gc01.html")>0 or url.find("docks-hubs/accessory/benq-hdmi-cable.html")>0:
            all_b2c_g6_url.append(url)
            all_b2c_g6_url_Result = {}
            all_b2c_g6_url_Result["URL"] = url
            all_b2c_g6_url_JSON.append(all_b2c_g6_url_Result)
    print(all_b2c_g6_url)
    print(len(all_b2c_g6_url))
    #generate csv
    current_dateTime = datetime.now()
    year=current_dateTime.year
    month=current_dateTime.month
    day=current_dateTime.day
    if month<10:
        month="0"+str(month)
    if day<10:
        day="0"+str(day)
    time = str(year) + str(month) + str(day)
    jsonFilename=time+"_all_b2c_g6_url.json"
    jsonLocation="./testreport/json/"
    with open(jsonLocation+jsonFilename, "w") as outfile:
        json.dump(all_b2c_g6_url_JSON, outfile)
    with open(jsonLocation+jsonFilename, "r") as f:
        json_data = json.load(f)
    # Convert JSON data to csv file
    csvFilename=time+"_all_b2c_g6_url.csv"
    csvLocation="./testreport/csv/"
    df = pd.json_normalize(json_data)
    df.to_csv(csvLocation+csvFilename, index=False)

#/speaker/
b2c_g5_speaker_url=[]
def filter_b2c_g5_speaker_url(driver: Chrome):
    b2c_g5_speaker_url_JSON=[]
    for i in range(0,len(url_b2c_all)):
        url=url_b2c_all[i]
        publishUrl = "/speaker/"
        if url.find("https://www.benq.com/en-us/speaker/") >=0:
            if url.find("accessory/")>=0 or url.find("desktop-dialogue-speaker/")>=0 or url.find("electrostatic-bluetooth-speaker/")>=0:
                if url.find("buy.html") < 0 and url.find("specifications.html") < 0 and url.find("question.html") < 0 and url.find("reviews.html") < 0 and url.find("spec.html") < 0:
                    b2c_g5_speaker_url.append(url)
                    b2c_g5_speaker_url_Result = {}
                    b2c_g5_speaker_url_Result["URL"] = url
                    b2c_g5_speaker_url_JSON.append(b2c_g5_speaker_url_Result)

    print(b2c_g5_speaker_url)
    print(len(b2c_g5_speaker_url))
    #generate csv
    current_dateTime = datetime.now()
    year=current_dateTime.year
    month=current_dateTime.month
    day=current_dateTime.day
    if month<10:
        month="0"+str(month)
    if day<10:
        day="0"+str(day)
    time = str(year) + str(month) + str(day)
    jsonFilename=time+"_b2c_g5_speaker_url.json"
    jsonLocation="./testreport/json/"
    with open(jsonLocation+jsonFilename, "w") as outfile:
        json.dump(b2c_g5_speaker_url_JSON, outfile)
    with open(jsonLocation+jsonFilename, "r") as f:
        json_data = json.load(f)
    # Convert JSON data to csv file
    csvFilename=time+"_b2c_g5_speaker_url.csv"
    csvLocation="./testreport/csv/"
    df = pd.json_normalize(json_data)
    df.to_csv(csvLocation+csvFilename, index=False)

#accessory
b2c_g5_accessory_url=[]
def filter_b2c_g5_accessory_url(driver: Chrome):
    b2c_g5_accessory_url_JSON=[]
    for i in range(0,len(url_b2c_all)):
        url=url_b2c_all[i]
        publishUrl = "/accessory/"
        if url.find("/accessory/") >=0:
            if url.find("buy.html") < 0 and url.find("specifications.html") < 0 and url.find("all.html") < 0 and url.find("question.html") < 0 and url.find("reviews.html") < 0 and url.find("spec.html") < 0 and url.find("docks-hubs") < 0 and url.find("gc01") < 0:
                b2c_g5_accessory_url.append(url)
                b2c_g5_accessory_url_Result = {}
                b2c_g5_accessory_url_Result["URL"] = url
                b2c_g5_accessory_url_JSON.append(b2c_g5_accessory_url_Result)

    print(b2c_g5_accessory_url)
    print(len(b2c_g5_accessory_url))
    #generate csv
    current_dateTime = datetime.now()
    year=current_dateTime.year
    month=current_dateTime.month
    day=current_dateTime.day
    if month<10:
        month="0"+str(month)
    if day<10:
        day="0"+str(day)
    time = str(year) + str(month) + str(day)
    jsonFilename=time+"_b2c_g5_accessory_url.json"
    jsonLocation="./testreport/json/"
    with open(jsonLocation+jsonFilename, "w") as outfile:
        json.dump(b2c_g5_accessory_url_JSON, outfile)
    with open(jsonLocation+jsonFilename, "r") as f:
        json_data = json.load(f)
    # Convert JSON data to csv file
    csvFilename=time+"_b2c_g5_accessory_url.csv"
    csvLocation="./testreport/csv/"
    df = pd.json_normalize(json_data)
    df.to_csv(csvLocation+csvFilename, index=False)

#projector-lamp /projector/lamps/
b2c_g5_projector_lamp_url=[]
def filter_b2c_g5_projector_lamp_url(driver: Chrome):
    b2c_g5_projector_lamp_url_JSON=[]
    for i in range(0,len(url_b2c_all)):
        url=url_b2c_all[i]
        publishUrl = "/projector/lamps/"
        if url.find("/projector/lamps/") >=0:
            if url.find("buy.html") < 0 and url.find("specifications.html") < 0 and url.find("question.html") < 0 and url.find("reviews.html") < 0 and url.find("spec.html") < 0:
                b2c_g5_projector_lamp_url.append(url)
                b2c_g5_projector_lamp_url_Result = {}
                b2c_g5_projector_lamp_url_Result["URL"] = url
                b2c_g5_projector_lamp_url_JSON.append(b2c_g5_projector_lamp_url_Result)

    print(b2c_g5_projector_lamp_url)
    print(len(b2c_g5_projector_lamp_url))
    #generate csv
    current_dateTime = datetime.now()
    year=current_dateTime.year
    month=current_dateTime.month
    day=current_dateTime.day
    if month<10:
        month="0"+str(month)
    if day<10:
        day="0"+str(day)
    time = str(year) + str(month) + str(day)
    jsonFilename=time+"_b2c_g5_projector_lamp_url.json"
    jsonLocation="./testreport/json/"
    with open(jsonLocation+jsonFilename, "w") as outfile:
        json.dump(b2c_g5_projector_lamp_url_JSON, outfile)
    with open(jsonLocation+jsonFilename, "r") as f:
        json_data = json.load(f)
    # Convert JSON data to csv file
    csvFilename=time+"_b2c_g5_projector_lamp_url.csv"
    csvLocation="./testreport/csv/"
    df = pd.json_normalize(json_data)
    df.to_csv(csvLocation+csvFilename, index=False)

#refurbished-computer-lights-lamps https://www.benq.com/en-us/refurbished-computer-lights-lamps/
b2c_g5_refurbished_computer_lights_lamp_url=[]
def filter_b2c_g5_refurbished_computer_lights_lamp_url(driver: Chrome):
    b2c_g5_refurbished_computer_lights_lamp_url_JSON=[]
    for i in range(0,len(url_b2c_all)):
        url=url_b2c_all[i]
        publishUrl = "https://www.benq.com/en-us/refurbished-computer-lights-lamps/"
        if url.find("https://www.benq.com/en-us/refurbished-computer-lights-lamps/") >=0:
            if url.find("refurbished-desk-lamps/") >=0 or url.find("refurbished-computer-monitor-lights/") >=0:
                if url.find("buy.html") < 0 and url.find("specifications.html") < 0 and url.find("question.html") < 0 and url.find("reviews.html") < 0 and url.find("spec.html") < 0:
                    b2c_g5_refurbished_computer_lights_lamp_url.append(url)
                    b2c_g5_refurbished_computer_lights_lamp_url_Result = {}
                    b2c_g5_refurbished_computer_lights_lamp_url_Result["URL"] = url
                    b2c_g5_refurbished_computer_lights_lamp_url_JSON.append(b2c_g5_refurbished_computer_lights_lamp_url_Result)

    print(b2c_g5_refurbished_computer_lights_lamp_url)
    print(len(b2c_g5_refurbished_computer_lights_lamp_url))
    #generate csv
    current_dateTime = datetime.now()
    year=current_dateTime.year
    month=current_dateTime.month
    day=current_dateTime.day
    if month<10:
        month="0"+str(month)
    if day<10:
        day="0"+str(day)
    time = str(year) + str(month) + str(day)
    jsonFilename=time+"_b2c_g5_refurbished_computer_lights_lamp.json"
    jsonLocation="./testreport/json/"
    with open(jsonLocation+jsonFilename, "w") as outfile:
        json.dump(b2c_g5_refurbished_computer_lights_lamp_url_JSON, outfile)
    with open(jsonLocation+jsonFilename, "r") as f:
        json_data = json.load(f)
    # Convert JSON data to csv file
    csvFilename=time+"_b2c_g5_refurbished_computer_lights_lamp.csv"
    csvLocation="./testreport/csv/"
    df = pd.json_normalize(json_data)
    df.to_csv(csvLocation+csvFilename, index=False)

#zowie
zowie_url=[]
def filter_zowie_url(driver: Chrome):
    zowie_url_JSON=[]
    for i in range(0,len(url_zowie_all)):
        url=url_zowie_all[i]
        publishUrl = "https://zowie.benq.com/en-us/"
        if url.find("https://zowie.benq.com/en-us/") >=0:
            if url.find("/camade/") >=0 or url.find("/mouse-skatez/") >=0 or url.find("/refurbished-gaming-esports-monitors/") >=0 or url.find("/monitor-accessory/") >=0 or url.find("/keyboard/") >=0 or url.find("/mouse-pad/") >=0 or url.find("https://zowie.benq.com/en-us/mouse/") >=0 or url.find("https://zowie.benq.com/en-us/monitor/") >=0 :
                if url.find("product-configure.html") < 0 and url.find("warranty.html") < 0 and url.find("video.html") < 0 and url.find("faq.html") < 0 and url.find("warranty.html") < 0 and url.find("configuration.html") < 0 and url.find("product-edition-compare-configure.html") < 0 and url.find("download.html") < 0 and url.find("comparison-config-page.html") < 0 and url.find("/knowledge/") < 0 and url.find("help-me-choose.html")< 0:
                    zowie_url.append(url)
                    zowie_url_Result = {}
                    zowie_url_Result["URL"] = url
                    zowie_url_JSON.append(zowie_url_Result)

    print(zowie_url)
    print(len(zowie_url))
    #generate csv
    current_dateTime = datetime.now()
    year=current_dateTime.year
    month=current_dateTime.month
    day=current_dateTime.day
    if month<10:
        month="0"+str(month)
    if day<10:
        day="0"+str(day)
    time = str(year) + str(month) + str(day)
    jsonFilename=time+"_zowie.json"
    jsonLocation="./testreport/json/"
    with open(jsonLocation+jsonFilename, "w") as outfile:
        json.dump(zowie_url_JSON, outfile)
    with open(jsonLocation+jsonFilename, "r") as f:
        json_data = json.load(f)
    # Convert JSON data to csv file
    csvFilename=time+"_zowie.csv"
    csvLocation="./testreport/csv/"
    df = pd.json_normalize(json_data)
    df.to_csv(csvLocation+csvFilename, index=False)

all_b2c_g6_result=[]
def test_b2c_g6_all_url(driver: Chrome):
    # url_all=["https://www.benq.com/en-us/monitor/professional/pd2705ua/refurbished-buy.html","https://www.benq.com/en-us/docks-hubs/accessory/benq-hdmi-cable.html","https://www.benq.com/en-us/projector/cinema/ht2150st/refurbished-buy.html","https://www.benq.com/en-us/projector/portable/gv1/buy.html","https://www.benq.com/en-us/monitor/gaming/ex3210u/refurbished-buy.html"]
    # for i in range(0,len(url_all)):
        # url=url_all[i]
    for i in range(0, len(all_b2c_g6_url)):
        url = all_b2c_g6_url[i]
        driver.get(url)
        driver.set_window_size(1500, 800)
        import time
        time.sleep(8)
        site_element="div.g6-site"
        site_element_all= driver.find_element(By.CSS_SELECTOR, site_element)
        html = site_element_all.get_attribute("innerHTML")
        # print(html)
        if html.find("buy-section") >=0  :
            if html.find("g6-btn-sm-solid-black") >=0:
                all_b2c_g6_dictResult = {}
                all_b2c_g6_dictResult["URL"] = url
                buybutton_element = "button.g6-btn-sm-solid-black"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g6_dictResult["Buy Button"] = buybutton_result
                all_b2c_g6_result.append(all_b2c_g6_dictResult)
                print(url,buybutton_result)
            elif html.find('<button class="discontinued">Discontinued</button>') >=0:
                all_b2c_g6_dictResult = {}
                all_b2c_g6_dictResult["URL"] = url
                buybutton_element = "button.discontinued"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g6_dictResult["Buy Button"] = buybutton_result
                all_b2c_g6_result.append(all_b2c_g6_dictResult)
                print(url, buybutton_result)
            else:
                all_b2c_g6_dictResult = {}
                all_b2c_g6_dictResult["URL"] = url
                buybutton_result="Fail"
                all_b2c_g6_dictResult["Buy Button"] = buybutton_result
                all_b2c_g6_result.append(all_b2c_g6_dictResult)
                print(url, buybutton_result)
        else:
            all_b2c_g6_dictResult = {}
            all_b2c_g6_dictResult["URL"] = url
            buybutton_result = "Fail"
            all_b2c_g6_dictResult["Buy Button"] = buybutton_result
            all_b2c_g6_result.append(all_b2c_g6_dictResult)
            print(url, buybutton_result)
    print(all_b2c_g6_result)
    #generate csv
    current_dateTime = datetime.now()
    year=current_dateTime.year
    month=current_dateTime.month
    day=current_dateTime.day
    if month<10:
        month="0"+str(month)
    if day<10:
        day="0"+str(day)
    today = str(year) + str(month) + str(day)
    jsonFilename="BQA Buy Button-"+today+"_G6_all.json"
    jsonLocation="./testreport/result/json/"
    with open(jsonLocation+jsonFilename, "w") as outfile:
        json.dump(all_b2c_g6_result, outfile)
    with open(jsonLocation+jsonFilename, "r") as f:
        json_data = json.load(f)
    # Convert JSON data to csv file
    csvFilename="BQA Buy Button-"+today+"_G6_all.csv"
    csvLocation="./testreport/result/csv/"
    df = pd.json_normalize(json_data)
    df.to_csv(csvLocation+csvFilename, index=False)

all_b2c_g5_speaker_result=[]
def test_b2c_g5_speaker_url(driver: Chrome):
    url_all=["https://www.benq.com/en-us/speaker/electrostatic-bluetooth-speaker/trevolos.html",
    "https://www.benq.com/en-us/speaker/electrostatic-bluetooth-speaker/trevolo-2.html" ,
    "https://www.benq.com/en-us/speaker/desktop-dialogue-speaker/trevolo-u.html" ,
    "https://www.benq.com/en-us/speaker/accessory/trevolo-u-carry-case.html" ,
    "https://www.benq.com/en-us/speaker/accessory/trevolo-stand.html" ,
    "https://www.benq.com/en-us/speaker/electrostatic-bluetooth-speaker/trevolo.html"]
    # for i in range(0,len(url_all)):
    #     url = url_all[i]
    for i in range(0, len(b2c_g5_speaker_url)):
        url = b2c_g5_speaker_url[i]
        driver.get(url)
        driver.set_window_size(1500, 800)
        import time
        time.sleep(5)
        site_element="div.component-products-right"
        site_element_all= driver.find_element(By.CSS_SELECTOR, site_element)
        html = site_element_all.get_attribute("innerHTML")
        # print(html)
        if html.find("main_buy") >=0  :
            if html.find('<span class="buy-now-btn">Buy Now</span>') >=0:
                all_b2c_g5_speaker_dictResult = {}
                all_b2c_g5_speaker_dictResult["URL"] = url
                buybutton_element = "span.buy-now-btn"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_speaker_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_speaker_result.append(all_b2c_g5_speaker_dictResult)
                print(url,buybutton_result)
            elif html.find('<div id="simpleFlow" class="button component-products-right-btn">Notify Me</div>') >=0:
                all_b2c_g5_speaker_dictResult = {}
                all_b2c_g5_speaker_dictResult["URL"] = url
                buybutton_element = "div.component-products-right-btn"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_speaker_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_speaker_result.append(all_b2c_g5_speaker_dictResult)
                print(url, buybutton_result)
            else:
                all_b2c_g5_speaker_dictResult = {}
                all_b2c_g5_speaker_dictResult["URL"] = url
                buybutton_result = "Fail"
                all_b2c_g5_speaker_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_speaker_result.append(all_b2c_g5_speaker_dictResult)
                print(url, buybutton_result)
        else:
            all_b2c_g5_speaker_dictResult = {}
            all_b2c_g5_speaker_dictResult["URL"] = url
            buybutton_result = "Fail"
            all_b2c_g5_speaker_dictResult["Buy Button"] = buybutton_result
            all_b2c_g5_speaker_result.append(all_b2c_g5_speaker_dictResult)
            print(url, buybutton_result)
    print(all_b2c_g5_speaker_result)
    #generate csv
    current_dateTime = datetime.now()
    year=current_dateTime.year
    month=current_dateTime.month
    day=current_dateTime.day
    if month<10:
        month="0"+str(month)
    if day<10:
        day="0"+str(day)
    today = str(year) + str(month) + str(day)
    jsonFilename="BQA Buy Button-"+today+"_G5_speaker.json"
    jsonLocation="./testreport/result/json/"
    with open(jsonLocation+jsonFilename, "w") as outfile:
        json.dump(all_b2c_g5_speaker_result, outfile)
    with open(jsonLocation+jsonFilename, "r") as f:
        json_data = json.load(f)
    # Convert JSON data to csv file
    csvFilename="BQA Buy Button-"+today+"_G5_speaker.csv"
    csvLocation="./testreport/result/csv/"
    df = pd.json_normalize(json_data)
    df.to_csv(csvLocation+csvFilename, index=False)

all_b2c_g5_accessory_result=[]
def test_b2c_g5_accessory_url(driver: Chrome):
    url_all=["https://www.benq.com/en-us/projector/accessory/carrybag-for-gv31.html",
    "https://www.benq.com/en-us/lighting/accessory/desk-clamp.html" ,
    "https://www.benq.com/en-us/projector/accessory/projector-remote-for-rci077.html" ,
    "https://www.benq.com/en-us/projector/accessory/carrybag-qs02.html"
    ]
    # for i in range(0,len(url_all)):
    #     url = url_all[i]
    for i in range(0, len(b2c_g5_accessory_url)):
        url = b2c_g5_accessory_url[i]
        driver.get(url)
        driver.set_window_size(1500, 800)
        import time
        time.sleep(5)
        site_element="html"
        site_element_all= driver.find_element(By.CSS_SELECTOR, site_element)
        html = site_element_all.get_attribute("innerHTML")
        # print(html)
        if html.find("main_buy") >=0  :
            if html.find('<span class="buy-now-btn">Buy Now</span>') >=0:
                all_b2c_g5_accessory_dictResult = {}
                all_b2c_g5_accessory_dictResult["URL"] = url
                buybutton_element = "span.buy-now-btn"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_accessory_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_accessory_result.append(all_b2c_g5_accessory_dictResult)
                print(url,buybutton_result)
            elif html.find('<div id="simpleFlow" class="button component-products-right-btn">Buy Now</div>') >=0:
                all_b2c_g5_accessory_dictResult = {}
                all_b2c_g5_accessory_dictResult["URL"] = url
                buybutton_element = "div.component-products-right-btn"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_accessory_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_accessory_result.append(all_b2c_g5_accessory_dictResult)
                print(url, buybutton_result)
            elif html.find('<div id="Not-simpleFlow" class="button component-products-right-btn enable-notify-me">Notify Me</div>') >=0:
                all_b2c_g5_accessory_dictResult = {}
                all_b2c_g5_accessory_dictResult["URL"] = url
                buybutton_element = "div.component-products-right-btn"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_accessory_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_accessory_result.append(all_b2c_g5_accessory_dictResult)
                print(url, buybutton_result)
            elif html.find('<div id="simpleFlow" class="button component-products-right-btn">Notify Me</div>') >=0:
                all_b2c_g5_accessory_dictResult = {}
                all_b2c_g5_accessory_dictResult["URL"] = url
                buybutton_element = "div.component-products-right-btn"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_accessory_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_accessory_result.append(all_b2c_g5_accessory_dictResult)
                print(url, buybutton_result)
            else:
                all_b2c_g5_accessory_dictResult = {}
                all_b2c_g5_accessory_dictResult["URL"] = url
                buybutton_result = "Fail"
                all_b2c_g5_accessory_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_accessory_result.append(all_b2c_g5_accessory_dictResult)
                print(url, buybutton_result)
        elif html.find("g6-local-navigation-container") >=0  :
            if html.find('<li class="button">') >=0:
                all_b2c_g5_accessory_dictResult = {}
                all_b2c_g5_accessory_dictResult["URL"] = url
                buybutton_element = "li.button"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_accessory_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_accessory_result.append(all_b2c_g5_accessory_dictResult)
                print(url,buybutton_result)
            else:
                all_b2c_g5_accessory_dictResult = {}
                all_b2c_g5_accessory_dictResult["URL"] = url
                buybutton_result = "Fail"
                all_b2c_g5_accessory_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_accessory_result.append(all_b2c_g5_accessory_dictResult)
                print(url, buybutton_result)
        else:
            all_b2c_g5_accessory_dictResult = {}
            all_b2c_g5_accessory_dictResult["URL"] = url
            buybutton_result = "Fail"
            all_b2c_g5_accessory_dictResult["Buy Button"] = buybutton_result
            all_b2c_g5_accessory_result.append(all_b2c_g5_accessory_dictResult)
            print(url, buybutton_result)
    print(all_b2c_g5_accessory_result)
    #generate csv
    current_dateTime = datetime.now()
    year=current_dateTime.year
    month=current_dateTime.month
    day=current_dateTime.day
    if month<10:
        month="0"+str(month)
    if day<10:
        day="0"+str(day)
    today = str(year) + str(month) + str(day)
    jsonFilename="BQA Buy Button-"+today+"_G5_accessory.json"
    jsonLocation="./testreport/result/json/"
    with open(jsonLocation+jsonFilename, "w") as outfile:
        json.dump(all_b2c_g5_accessory_result, outfile)
    with open(jsonLocation+jsonFilename, "r") as f:
        json_data = json.load(f)
    # Convert JSON data to csv file
    csvFilename="BQA Buy Button-"+today+"_G5_accessory.csv"
    csvLocation="./testreport/result/csv/"
    df = pd.json_normalize(json_data)
    df.to_csv(csvLocation+csvFilename, index=False)

all_b2c_g5_projector_lamp_result=[]
def test_b2c_g5_projector_lamp_url(driver: Chrome):
    url_all=["https://www.benq.com/en-us/projector/lamps/lamp-for-mx520.html",
    "https://www.benq.com/en-us/projector/lamps/lamp-for-ms510.html" ,
    "https://www.benq.com/en-us/projector/lamps/lamp-for-w6000.html" ]
    # for i in range(0,len(url_all)):
    #     url = url_all[i]
    for i in range(0, len(b2c_g5_projector_lamp_url)):
        url = b2c_g5_projector_lamp_url[i]
        driver.get(url)
        driver.set_window_size(1500, 800)
        import time
        time.sleep(5)
        site_element="div.component-products-right"
        site_element_all= driver.find_element(By.CSS_SELECTOR, site_element)
        html = site_element_all.get_attribute("innerHTML")
        # print(html)
        if html.find("main_buy") >=0  :
            if html.find('<span class="buy-now-btn">Buy Now</span>') >=0:
                all_b2c_g5_projector_lamp_dictResult = {}
                all_b2c_g5_projector_lamp_dictResult["URL"] = url
                buybutton_element = "span.buy-now-btn"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_projector_lamp_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_projector_lamp_result.append(all_b2c_g5_projector_lamp_dictResult)
                print(url,buybutton_result)
            elif html.find('<div id="simpleFlow" class="button component-products-right-btn">Buy Now</div>') >=0:
                all_b2c_g5_projector_lamp_dictResult = {}
                all_b2c_g5_projector_lamp_dictResult["URL"] = url
                buybutton_element = "div.component-products-right-btn"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_projector_lamp_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_projector_lamp_result.append(all_b2c_g5_projector_lamp_dictResult)
                print(url, buybutton_result)
            elif html.find('<div id="Not-simpleFlow" class="button component-products-right-btn enable-notify-me">Notify Me</div>') >=0:
                all_b2c_g5_projector_lamp_dictResult = {}
                all_b2c_g5_projector_lamp_dictResult["URL"] = url
                buybutton_element = "div.component-products-right-btn"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_projector_lamp_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_projector_lamp_result.append(all_b2c_g5_projector_lamp_dictResult)
                print(url, buybutton_result)
            elif html.find('<div id="simpleFlow" class="button component-products-right-btn">Notify Me</div>') >=0:
                all_b2c_g5_projector_lamp_dictResult = {}
                all_b2c_g5_projector_lamp_dictResult["URL"] = url
                buybutton_element = "div.component-products-right-btn"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_projector_lamp_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_projector_lamp_result.append(all_b2c_g5_projector_lamp_dictResult)
                print(url, buybutton_result)
            else:
                all_b2c_g5_projector_lamp_dictResult = {}
                all_b2c_g5_projector_lamp_dictResult["URL"] = url
                buybutton_result = "Fail"
                all_b2c_g5_projector_lamp_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_projector_lamp_result.append(all_b2c_g5_projector_lamp_dictResult)
                print(url, buybutton_result)
        else:
            all_b2c_g5_projector_lamp_dictResult = {}
            all_b2c_g5_projector_lamp_dictResult["URL"] = url
            buybutton_result = "Fail"
            all_b2c_g5_projector_lamp_dictResult["Buy Button"] = buybutton_result
            all_b2c_g5_projector_lamp_result.append(all_b2c_g5_projector_lamp_dictResult)
            print(url, buybutton_result)
    print(all_b2c_g5_projector_lamp_result)
    #generate csv
    current_dateTime = datetime.now()
    year=current_dateTime.year
    month=current_dateTime.month
    day=current_dateTime.day
    if month<10:
        month="0"+str(month)
    if day<10:
        day="0"+str(day)
    today = str(year) + str(month) + str(day)
    jsonFilename="BQA Buy Button-"+today+"_G5_projector_lamp.json"
    jsonLocation="./testreport/result/json/"
    with open(jsonLocation+jsonFilename, "w") as outfile:
        json.dump(all_b2c_g5_projector_lamp_result, outfile)
    with open(jsonLocation+jsonFilename, "r") as f:
        json_data = json.load(f)
    # Convert JSON data to csv file
    csvFilename="BQA Buy Button-"+today+"_G5_projector_lamp.csv"
    csvLocation="./testreport/result/csv/"
    df = pd.json_normalize(json_data)
    df.to_csv(csvLocation+csvFilename, index=False)

all_b2c_g5_refurbished_computer_lights_lamp_result=[]
def test_b2c_g5_refurbished_computer_lights_lamp_url(driver: Chrome):
    url_all=["https://www.benq.com/en-us/refurbished-computer-lights-lamps/refurbished-computer-monitor-lights/screenbar-plus.html ",
    "https://www.benq.com/en-us/refurbished-computer-lights-lamps/refurbished-desk-lamps/floorlamp-refurbished.html" ,
    "https://www.benq.com/en-us/refurbished-computer-lights-lamps/refurbished-desk-lamps/desklamp-genie-refurbished.html",
    "https://www.benq.com/en-us/refurbished-computer-lights-lamps/refurbished-desk-lamps/desklamp-refurbished.html"]
    # for i in range(0,len(url_all)):
    #     url = url_all[i]
    for i in range(0, len(b2c_g5_refurbished_computer_lights_lamp_url)):
        url = b2c_g5_refurbished_computer_lights_lamp_url[i]
        driver.get(url)
        driver.set_window_size(1500, 800)
        import time
        time.sleep(5)
        site_element="div.component-products-right"
        site_element_all= driver.find_element(By.CSS_SELECTOR, site_element)
        html = site_element_all.get_attribute("innerHTML")
        # print(html)
        if html.find("main_buy") >=0  :
            if html.find('<span class="buy-now-btn">Buy Now</span>') >=0:
                all_b2c_g5_refurbished_computer_lights_lamp_dictResult = {}
                all_b2c_g5_refurbished_computer_lights_lamp_dictResult["URL"] = url
                buybutton_element = "span.buy-now-btn"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_refurbished_computer_lights_lamp_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_refurbished_computer_lights_lamp_result.append(all_b2c_g5_refurbished_computer_lights_lamp_dictResult)
                print(url,buybutton_result)
            elif html.find('<div id="simpleFlow" class="button component-products-right-btn">Notify Me</div>') >=0:
                all_b2c_g5_refurbished_computer_lights_lamp_dictResult = {}
                all_b2c_g5_refurbished_computer_lights_lamp_dictResult["URL"] = url
                buybutton_element = "div.component-products-right-btn"
                buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                buybutton_result=buybutton_element_all.text
                all_b2c_g5_refurbished_computer_lights_lamp_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_refurbished_computer_lights_lamp_result.append(all_b2c_g5_refurbished_computer_lights_lamp_dictResult)
                print(url, buybutton_result)
            else:
                all_b2c_g5_refurbished_computer_lights_lamp_dictResult = {}
                all_b2c_g5_refurbished_computer_lights_lamp_dictResult["URL"] = url
                buybutton_result = "Fail"
                all_b2c_g5_refurbished_computer_lights_lamp_dictResult["Buy Button"] = buybutton_result
                all_b2c_g5_refurbished_computer_lights_lamp_result.append(all_b2c_g5_refurbished_computer_lights_lamp_dictResult)
                print(url, buybutton_result)
        else:
            all_b2c_g5_refurbished_computer_lights_lamp_dictResult = {}
            all_b2c_g5_refurbished_computer_lights_lamp_dictResult["URL"] = url
            buybutton_result = "Fail"
            all_b2c_g5_refurbished_computer_lights_lamp_dictResult["Buy Button"] = buybutton_result
            all_b2c_g5_refurbished_computer_lights_lamp_result.append(all_b2c_g5_refurbished_computer_lights_lamp_dictResult)
            print(url, buybutton_result)
    print(all_b2c_g5_refurbished_computer_lights_lamp_result)
    #generate csv
    current_dateTime = datetime.now()
    year=current_dateTime.year
    month=current_dateTime.month
    day=current_dateTime.day
    if month<10:
        month="0"+str(month)
    if day<10:
        day="0"+str(day)
    today = str(year) + str(month) + str(day)
    jsonFilename="BQA Buy Button-"+today+"_G5_refurbished_computer_lights_lamp.json"
    jsonLocation="./testreport/result/json/"
    with open(jsonLocation+jsonFilename, "w") as outfile:
        json.dump(all_b2c_g5_refurbished_computer_lights_lamp_result, outfile)
    with open(jsonLocation+jsonFilename, "r") as f:
        json_data = json.load(f)
    # Convert JSON data to csv file
    csvFilename="BQA Buy Button-"+today+"_G5_refurbished_computer_lights_lamp.csv"
    csvLocation="./testreport/result/csv/"
    df = pd.json_normalize(json_data)
    df.to_csv(csvLocation+csvFilename, index=False)

zowie_result=[]
def test_zowie_url(driver: Chrome):
    url_all=["https://zowie.benq.com/en-us/mouse-pad/g-sr-ii.html","https://zowie.benq.com/en-us/mouse-pad/p-sr.html","https://zowie.benq.com/en-us/mouse/za11-b-white.html"]
    # for i in range(0,len(url_all)):
    #     url=url_all[i]
    for i in range(1, len(zowie_url)):
        url = zowie_url[i]
        driver.get(url)
        driver.set_window_size(1500, 800)
        import time
        time.sleep(5)
        check_element="body.gaming-site"
        check_element_all = driver.find_element(By.CSS_SELECTOR, check_element)
        check_html = check_element_all.get_attribute("innerHTML")
        if check_html.find("game-product-info")<0:
            zowie_dictResult = {}
            zowie_dictResult["URL"] = url
            buybutton_result= "Fail"
            zowie_dictResult["Buy Button"] = buybutton_result
            zowie_result.append(zowie_dictResult)
            print(url, buybutton_result)
        else:
            site_element="section.game-product-info"
            site_element_all= driver.find_element(By.CSS_SELECTOR, site_element)
            html = site_element_all.get_attribute("innerHTML")
            # print(html)
            if html.find("game-product-info-purchase-btn") >=0  :
                if html.find('<button class="add-to-cart">Add To Cart</button>') >=0:
                    zowie_dictResult = {}
                    zowie_dictResult["URL"] = url
                    buybutton_element = "button.add-to-cart"
                    buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                    buybutton_result=buybutton_element_all.text
                    zowie_dictResult["Buy Button"] = buybutton_result
                    zowie_result.append(zowie_dictResult)
                    print(url,buybutton_result)
                elif html.find('Notify Me</button>') >=0:
                    zowie_dictResult = {}
                    zowie_dictResult["URL"] = url
                    buybutton_element = "button.notify"
                    buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                    buybutton_result=buybutton_element_all.text
                    zowie_dictResult["Buy Button"] = buybutton_result
                    zowie_result.append(zowie_dictResult)
                    print(url, buybutton_result)
                elif html.find('<button class="buy-btn sold-out">Discontinued</button>') >=0:
                    zowie_dictResult = {}
                    zowie_dictResult["URL"] = url
                    buybutton_element = "button.sold-out"
                    buybutton_element_all = driver.find_element(By.CSS_SELECTOR, buybutton_element)
                    buybutton_result=buybutton_element_all.text
                    zowie_dictResult["Buy Button"] = buybutton_result
                    zowie_result.append(zowie_dictResult)
                    print(url, buybutton_result)
                else:
                    zowie_dictResult = {}
                    zowie_dictResult["URL"] = url
                    buybutton_result="Fail"
                    zowie_dictResult["Buy Button"] = buybutton_result
                    zowie_result.append(zowie_dictResult)
                    print(url, buybutton_result)
            else:
                zowie_dictResult = {}
                zowie_dictResult["URL"] = url
                buybutton_result = "Fail"
                zowie_dictResult["Buy Button"] = buybutton_result
                zowie_result.append(zowie_dictResult)
                print(url, buybutton_result)
    print(zowie_result)
    #generate csv
    current_dateTime = datetime.now()
    year=current_dateTime.year
    month=current_dateTime.month
    day=current_dateTime.day
    if month<10:
        month="0"+str(month)
    if day<10:
        day="0"+str(day)
    today = str(year) + str(month) + str(day)
    jsonFilename="BQA Buy Button-"+today+"_zowie.json"
    jsonLocation="./testreport/result/json/"
    with open(jsonLocation+jsonFilename, "w") as outfile:
        json.dump(zowie_result, outfile)
    with open(jsonLocation+jsonFilename, "r") as f:
        json_data = json.load(f)
    # Convert JSON data to csv file
    csvFilename="BQA Buy Button-"+today+"_zowie.csv"
    csvLocation="./testreport/result/csv/"
    df = pd.json_normalize(json_data)
    df.to_csv(csvLocation+csvFilename, index=False)


if __name__=='__main__':
    with create_chrome_driver() as chrome_driver:
        get_all_b2c_url(driver=chrome_driver)
        get_all_zowie_url(driver=chrome_driver)
        filter_all_b2c_g6_url(driver=chrome_driver)
        filter_b2c_g5_speaker_url(driver=chrome_driver)
        filter_b2c_g5_accessory_url(driver=chrome_driver)
        filter_b2c_g5_projector_lamp_url(driver=chrome_driver)
        filter_b2c_g5_refurbished_computer_lights_lamp_url(driver=chrome_driver)
        filter_zowie_url(driver=chrome_driver)
        test_b2c_g6_all_url(driver=chrome_driver)
        test_b2c_g5_speaker_url(driver=chrome_driver)
        test_b2c_g5_accessory_url(driver=chrome_driver)
        test_b2c_g5_projector_lamp_url(driver=chrome_driver)
        test_b2c_g5_refurbished_computer_lights_lamp_url(driver=chrome_driver)
        test_zowie_url(driver=chrome_driver)
        chrome_driver.quit()

