# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 10:13:17 2023

@author: Celine.HY.Chiu
"""


allURL=[
  {
    "URL": "application/docking-station-faq-kn-00005",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/docking-station-faq-kn-00008",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/docking-station-faq-kn-00061",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00007",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/docking-station-faq-kn-00005",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00003",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/docking-station-faq-kn-00001",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00039",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00040",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00041",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00044",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00046",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00047",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00048",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00049",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00050",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00051",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00052",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00053",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00054",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00055",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00056",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00057",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00058",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00059",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/ltv-faq-kn-00060",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-k-00056",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00013",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00029",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00047",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00050",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00051",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00052",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00053",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00055",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00056",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00057",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00058",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00060",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00061",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00062",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00063",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/monitor-faq-kn-00064",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/projector-faq-k-00084",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/projector-faq-k-00164",
    "Regions": "zh-tw"
  },
  {
    "URL": "application/projector-faq-kn-00076",
    "Regions": "zh-tw"
  },
  {
    "URL": "explanation/e-reading-lamp-faq-kn-00041",
    "Regions": "zh-tw"
  },
  {
    "URL": "explanation/e-reading-lamp-faq-kn-00042",
    "Regions": "zh-tw"
  },
  {
    "URL": "explanation/e-reading-lamp-faq-kn-00043",
    "Regions": "zh-tw"
  },
  {
    "URL": "explanation/monitor-faq-k-00004",
    "Regions": "zh-tw"
  },
  {
    "URL": "explanation/monitor-faq-k-00023",
    "Regions": "zh-tw"
  },
  {
    "URL": "explanation/monitor-faq-kn-00021",
    "Regions": "zh-tw"
  },
  {
    "URL": "explanation/monitor-faq-kn-00022",
    "Regions": "zh-tw"
  },
  {
    "URL": "explanation/monitor-faq-kn-00023",
    "Regions": "zh-tw"
  },
  {
    "URL": "explanation/monitor-faq-kn-00024",
    "Regions": "zh-tw"
  },
  {
    "URL": "explanation/monitor-faq-kn-00025",
    "Regions": "zh-tw"
  },
  {
    "URL": "explanation/projector-faq-kn-00032",
    "Regions": "zh-tw"
  },
  {
    "URL": "explanation/projector-faq-kn-00034",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00001",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00002",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00003",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00004",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00005",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00006",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00007",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00008",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00009",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00010",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00011",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00012",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00013",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00014",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00015",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00016",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00017",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00018",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00019",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00020",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00021",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00022",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00023",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00024",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00025",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/docking-station-faq-kn-00026",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00010",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00011",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00012",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00013",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00014",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00015",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00016",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00017",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00018",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00019",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00020",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00021",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00022",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00023",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00024",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00025",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/ltv-faq-kn-00026",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/monitor-faq-kn-00021",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/monitor-faq-kn-00022",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/projector-faq-kn-00091",
    "Regions": "zh-tw"
  },
  {
    "URL": "specification/trevolo-faq-kn-00011",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/e-reading-lamp-faq-kn-00001",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/ltv-faq-kn-00031",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/ltv-faq-kn-00032",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/ltv-faq-kn-00033",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/ltv-faq-kn-00034",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/ltv-faq-kn-00035",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/monitor-faq-k-00012",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/monitor-faq-kn-00016",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/monitor-faq-kn-00017",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/monitor-faq-kn-00018",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/monitor-faq-kn-00019",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/monitor-faq-kn-00020",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/monitor-faq-kn-00021",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/projector-faq-kn-00040",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/projector-faq-kn-00041",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/projector-faq-kn-00052",
    "Regions": "zh-tw"
  },
  {
    "URL": "troubleshooting/projector-faq-kn-00053",
    "Regions": "zh-tw"
  }
]

import requests
import json
import pandas as pd

result=[]


for i in range(0,len(allURL)):
    dict=allURL[i]
    #print(dict["URL"])
    server="https://www.benq.com/"
    countryCode="zh-tw"
    faq="/support/downloads-faq/faq/product/"
    #urlName="application/e-reading-lamp-faq-kn-00003.html"
    urlName=dict["URL"]
    url=server+countryCode+faq+urlName
    print(url)
    print(requests.get(url).status_code)
    dictResult={}
    dictResult["country code"]="zh-tw"
    dictResult["URL name"]=urlName
    dictResult["URL"]=url
    dictResult["status code"]=requests.get(url).status_code
    if requests.get(url).status_code==200:
        dictResult["result"]="Pass"
    else:
        dictResult["result"]="Fail"    
    result.append( dictResult )
    
#jsondict = json.dumps(result)
#print(jsondict)
    

# file name is mydata
with open("faqCrawler-zhtw-TestResult.json", "w") as final:
    json.dump(result, final)
    
# Reading JSON data from a file
with open("faqCrawler-zhtw-TestResult.json", "r") as f:
    json_data = json.load(f)
    
# Convert JSON data to csv file
df = pd.json_normalize(json_data)
df.to_csv("faqCrawler-zhtw-TestResult.csv", index=False)