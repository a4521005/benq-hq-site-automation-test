# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 10:15:25 2023

@author: Celine.HY.Chiu
"""

allURL=[
  {
    "URL": "application/cam-faq-kn-00014"
  },
  {
    "URL": "application/cam-faq-kn-00018"
  },
  {
    "URL": "application/cam-faq-kn-00019"
  },
  {
    "URL": "application/cam-faq-kn-00020"
  },
  {
    "URL": "application/cam-faq-kn-00021"
  },
  {
    "URL": "application/cam-faq-kn-00022"
  },
  {
    "URL": "application/cam-faq-kn-00023"
  },
  {
    "URL": "application/cam-faq-kn-00024"
  },
  {
    "URL": "application/cam-faq-kn-00025"
  },
  {
    "URL": "application/cam-faq-kn-00026"
  }
]

#ZH-TW, EN-US, EN-AU, JA-JP, EN-IN, EN-AP
countryCodeAll=["zh-tw","en-us","en-au","ja-jp","en-in","en-ap"]

import requests
import json
import pandas as pd

result=[]


for i in range(0,len(allURL)):
    dict=allURL[i]
    #print(dict["URL"])
    server="https://www.benq.com/"
    faq="/support/downloads-faq/faq/product/"
    urlName=dict["URL"]
    for j in range(0,len(countryCodeAll)):
        countryCode=countryCodeAll[j]                
        url=server+countryCode+faq+urlName
        #print(url)
        #print(requests.get(url).status_code)
        dictResult={}
        dictResult["country code"]=countryCode
        dictResult["URL name"]=urlName
        dictResult["URL"]=url
        dictResult["status code"]=requests.get(url).status_code
        if requests.get(url).status_code==200:
            dictResult["result"]="Pass"
        else:
            dictResult["result"]="Fail"
        result.append( dictResult )
    
#jsondict = json.dumps(result)
#print(jsondict)
    

# file name is mydata
with open("faqCrawler-ZH-TW, EN-US, EN-AU, JA-JP, EN-IN, EN-AP.json", "w") as final:
    json.dump(result, final)
    
# Reading JSON data from a file
with open("faqCrawler-ZH-TW, EN-US, EN-AU, JA-JP, EN-IN, EN-AP.json", "r") as f:
    json_data = json.load(f)
    
# Convert JSON data to csv file
df = pd.json_normalize(json_data)
df.to_csv("faqCrawler-ZH-TW, EN-US, EN-AU, JA-JP, EN-IN, EN-AP-TestResult.csv", index=False)

